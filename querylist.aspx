<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html dir="ltr" xmlns="http://www.w3.org/1999/xhtml">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>
	Sin título 1
</title>

<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.1.min.js"></script>

</head>

<body>

<div>
	<div>Leer de una lista filtrada</div>
	<select id="tiposelect" onchange="listquery($(this).val())"><option>Selecciona el tipo</option><option>Tipo1</option><option>Tipo2</option></select>
	<table border="1" id="tabla">
		<tr><th>ID</th><th>Title</th><th>Body</th><th>Expires</th></tr>
	</table>
	
	<div style="margin-top:20px">Editar el campo "Title" un item de una lista segun su ID</div>
	<div style="width:210px;"><div style="width:160px;float:left">Title</div><div style="width:50px;float:right">ID</div></div>
	<div><input id="titleaeditar" type="text"/><input id="idaeditar" style="width:50px" type="text"/><button onclick="editarunitem()">Guardar</button></div>
	
	<div style="margin-top:20px">Crear un item en una lista</div>
	<div style="width:460px;"><div style="width:160px;float:left">Title</div><div style="width:150px;float:left">Body</div><div style="width:150px;float:right">Tipo</div></div>
	<div><input id="titleacrear" type="text"/><input id="bodyacrear" style="width:150px" type="text"/><select id="tipoacrear"><option>Tipo1</option><option>Tipo2</option></select><button onclick="crearunitem()">Guardar</button></div>
	
	<div style="margin-top:20px">Obtener el nombre del grupo al que el usuario actual pertenece</div>
	<div><input id="grupo" type="text"/><button onclick="obtenernombredelusuario();obtenergrupodelusuario();">GO</button></div>
	
</div>

<script type="text/javascript">

function listquery(tipo){

//var count = 0;
$(".row").remove();

var soapEnv =
            "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/'> \
                <soapenv:Body> \
                     <GetListItems xmlns='http://schemas.microsoft.com/sharepoint/soap/'> \
                      <listName>Announcements</listName> \
                      <query><Query><Where><Eq><FieldRef Name='Tipo' /><Value Type='Text'>" + tipo + "</Value></Eq></Where></Query></query> \
                        <viewFields> \
                            <ViewFields> \
                              <FieldRef Name='ID' /> \
                              <FieldRef Name='Title' /> \
                              <FieldRef Name='Body' /> \
                              <FieldRef Name='Tipo' /> \
                              <FieldRef Name='Expires' /> \
                            </ViewFields> \
                        </viewFields> \
                        <rowLimit>" + 0 + "</rowLimit> \
                        </GetListItems> \
                </soapenv:Body> \
            </soapenv:Envelope>";
            
            $.ajax({
            url: "http://aprende.ignitek.com/_vti_bin/lists.asmx",
            type: "POST",
            async: false,
            dataType: "xml",
            data: soapEnv,
            complete: function(xData, status) {

	//count = parseInt($(xData.responseXML).find('*').filter(function() {return this.nodeName === "rs:data";  }).attr("ItemCount"));
	$(xData.responseXML).find('*').filter(function() {return this.nodeName === "z:row";  }).each(function() {
	
		console.log("el id es: " + $(this).attr("ows_ID"));
		$("#tabla").append('<tr class="row"><td>' + $(this).attr("ows_ID") + '</td><td>' + $(this).attr("ows_Title") + '</td><td>' + $(this).attr("ows_Body") + '</td><td>' + $(this).attr("ows_Expires") + '</td></tr>');
		
	});
	
	
 }
,
            contentType: "text/xml; charset=\"utf-8\""
        });
        

}


function editarunitem(){

	var batch =
        "<Batch OnError=\"Continue\"> \
            <Method ID=\"1\" Cmd=\"Update\"> \
            <Field Name='ID'>" + $("#idaeditar").val() + "</Field>\
			<Field Name=\"Title\">" + $("#titleaeditar").val() + "</Field> \
            </Method> \
        </Batch>";

	var soapEnv =
        "<?xml version=\"1.0\" encoding=\"utf-8\"?> \
        <soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" \
            xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" \
            xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"> \
          <soap:Body> \
            <UpdateListItems xmlns=\"http://schemas.microsoft.com/sharepoint/soap/\"> \
              <listName>Announcements</listName> \
              <updates> \
                " + batch + "</updates> \
            </UpdateListItems> \
          </soap:Body> \
        </soap:Envelope>";

    $.ajax({
        url: "http://aprende.ignitek.com/_vti_bin/lists.asmx",
        beforeSend: function(xhr) {            xhr.setRequestHeader("SOAPAction",

            "http://schemas.microsoft.com/sharepoint/soap/UpdateListItems");
        },
        type: "POST",
        async:false,
        dataType: "xml",
        data: soapEnv,
        contentType: "text/xml; charset=utf-8"
    });
    
	listquery($("#tiposelect").val());
	
}

function crearunitem(){

	var batch =
        "<Batch OnError=\"Continue\"> \
            <Method ID=\"1\" Cmd=\"New\"> \
            <Field Name='Body'>" + $("#bodyacrear").val() + "</Field>\
			<Field Name=\"Title\">" + $("#titleacrear").val() + "</Field> \
			<Field Name=\"Tipo\">" + $("#tipoacrear").val() + "</Field> \
            </Method> \
        </Batch>";

	var soapEnv =
        "<?xml version=\"1.0\" encoding=\"utf-8\"?> \
        <soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" \
            xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" \
            xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"> \
          <soap:Body> \
            <UpdateListItems xmlns=\"http://schemas.microsoft.com/sharepoint/soap/\"> \
              <listName>Announcements</listName> \
              <updates> \
                " + batch + "</updates> \
            </UpdateListItems> \
          </soap:Body> \
        </soap:Envelope>";

    $.ajax({
        url: "http://aprende.ignitek.com/_vti_bin/lists.asmx",
        beforeSend: function(xhr) {            xhr.setRequestHeader("SOAPAction",

            "http://schemas.microsoft.com/sharepoint/soap/UpdateListItems");
        },
        type: "POST",
        async:false,
        dataType: "xml",
        data: soapEnv,
        contentType: "text/xml; charset=utf-8"
    });
    
	listquery($("#tiposelect").val());
}

function obtenergrupodelusuario(){
       
		 var soapEnv =
            "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/'> \
                <soapenv:Body> \
                     <GetGroupCollectionFromUser xmlns='http://schemas.microsoft.com/sharepoint/soap/directory/'> \
                        <userLoginName>" + usuario + "</userLoginName> \
                         </GetGroupCollectionFromUser> \
                </soapenv:Body> \
            </soapenv:Envelope>";

         
         $.ajax({
            url: "http://aprende.ignitek.com/_vti_bin/UserGroup.asmx",
            type: "POST",
            async: false,
            dataType: "xml",
            data: soapEnv,
            complete: function(xData, status) {var grupodelusuario = [];

 $(xData.responseXML).find('*').filter(function() {return this.nodeName === "Groups";  }).each(function() {
        
        $(this).find("Group").each(function() {
        
        grupodelusuario = $(this).attr("Name");
       
 		});
  });
  
  $("#grupo").val(grupodelusuario);
  
}
,
            contentType: "text/xml; charset=\"utf-8\""
        });
}

function obtenernombredelusuario(){

var thisTextValue = RegExp("FieldInternalName=\"" + "Title" + "\"", "gi");
var thisTextValue2 = RegExp("FieldInternalName=\"" + "Name" + "\"", "gi");

		 $.ajax({
			
			async: false,
			url: "http://aprende.ignitek.com/_layouts/userdisp.aspx?Force=True&" + new Date().getTime(),
			complete: function (xData, Status) {		

				$(xData.responseText).find("table.ms-formtable td[id^='SPField']").each(function() {
					if(thisTextValue.test($(this).html())) {
						usuario2 = $(this).text();						
					}	
					if(thisTextValue2.test($(this).html())) {
						usuario = $(this).text();
							
					}			
				});

			usuario2 = usuario2.replace(/(^[\s\xA0]+|[\s\xA0]+$)/g, '');
			usuario = usuario.replace(/(^[\s\xA0]+|[\s\xA0]+$)/g, '');	

					
			}
		});	

		
}

</script>

</body>

</html>
