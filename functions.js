var script = document.createElement('script');
script.src = 'https://dl.dropboxusercontent.com/u/61331142/jquery.SPServices-2014.01.min.js';
script.type = 'text/javascript';
$('head')[0].appendChild(script);
var debug;

var Products = {
  buildBanner: function (bannerContainer, selectorContainer){
    bannerContainer = typeof bannerContainer !== 'undefined' ? $(bannerContainer) : $('.carousel-inner');
    selectorContainer = typeof selectorContainer !== 'undefined' ? $(selectorContainer) : $('header.carrusel-play').find('ul.list-inline');
    $().SPServices({
      operation: "GetListItems",
      async: false,
      listName: "Productos",
      CAMLQuery: " <Query><Where><Eq><FieldRef Name='Clasificacion' /><Value Type='Text'>Play 4 Results</Value></Eq></Where></Query>",
      CAMLViewFields: "<ViewFields><FieldRef Name='Title' /><FieldRef Name='Descripcion' /><FieldRef Name='Banner' /><FieldRef Name='Clasificacion' /><FieldRef Name='link' /><FieldRef Name='Thumbnail' /></ViewFields>",
      completefunc: function (xData, Status) {
        $(xData.responseXML).SPFilterNode("z:row").each(function(index) {
          var image_url = typeof ($(this).attr("ows_Banner")) !== "undefined" ?  $(this).attr("ows_Banner").replace(', ','') : 'http://lorempixel.com/750/180/business';
          var thumbnail_url = typeof ($(this).attr("ows_Thumbnail")) !== "undefined" ?  $(this).attr("ows_Thumbnail").replace(', ','') : 'http://lorempixel.com/163/60/business';
          var itemDiv = $('<div />',{
            class: index === 0 ? 'active item' : 'item',
            });
          itemDiv.attr("data-slide-number", index);
          var img = $('<img />',{
            class: 'img-responsive'
            });
          img.attr("src",image_url);
          itemDiv.append(img);
          itemDiv.append(Products.BannerButtons);

          var selector = $('<li />');
          var a = $('<a />',{
            id: "carousel-selector-"+index,
            class: "selected"
          });
          var thumbnail = $('<img />',{
            class: "img-responsive"
          });
          thumbnail.attr('src',thumbnail_url);
          thumbnail.attr('height',60);
          thumbnail.attr('width',163);
          selector.append(a.append(thumbnail));

          selectorContainer.append(selector);
          bannerContainer.append(itemDiv);
        });
      }
    });
  },
  BannerButtons: '<div class="carousel-caption"><a class="btn btn-lg btn-primary btn-slide-prog" href="#">Detalle</a>&nbsp;<a class="btn btn-lg btn-primary btn-slide-prog" href="#">Trailer</a><br><br></div>',
};
var Filter = {
  skill: function (topic) {
    $().SPServices({
      operation: "GetListItems",
      async: false,
      listName: "SubProductos",
      CAMLQuery: "<Query><Where><Eq><FieldRef Name='Tema' /><Value Type='Text'>" + topic + "</Value></Eq></Where></Query>",
      CAMLViewFields: "<ViewFields><FieldRef Name='ID' /><FieldRef Name='Title' /><FieldRef Name='Producto' /><FieldRef Name='Imagen' /><FieldRef Name='Tema' /></ViewFields>",
      completefunc: function (xData, Status) {
        console.log(Status);
        $(xData.responseXML).SPFilterNode("z:row").each(function(index) {
          console.log($(this).attr("ows_Title"));
        });
      }
    });
  },
  activity: function (pT) {
    $().SPServices({
      operation: "GetListItems",
      async: false,
      listName: "SubProductos",
      CAMLQuery: "<Query><Where><Eq><FieldRef Name='Title' /><Value Type='Text'>" + pT + "</Value></Eq></Where></Query>",
      CAMLViewFields: "<ViewFields><FieldRef Name='ID' /><FieldRef Name='Title' /><FieldRef Name='Producto' /><FieldRef Name='Imagen' /><FieldRef Name='Tema' /></ViewFields>",
      completefunc: function (xData, Status) {
        console.log(Status);
        console.log(xData.responseXML);
      }
    });
  },
  product: function (pT) {
    $().SPServices({
      operation: "GetListItems",
      async: false,
      listName: "SubProductos",
      CAMLQuery: "<Query><Where><Eq><FieldRef Name='Producto' /><Value Type='Text'>" + pT + "</Value></Eq></Where></Query>",
      CAMLViewFields: "<ViewFields><FieldRef Name='ID' /><FieldRef Name='Title' /><FieldRef Name='Producto' /><FieldRef Name='Imagen' /><FieldRef Name='Tema' /></ViewFields>",
      completefunc: function (xData, Status) {
        console.log(Status);
        console.log($(xData.responseXML).SPFilterNode("z:row"));
      }
    });
  }
};
var Booking = {
  default: function () {
    console.log("initialize");
  }
};
var User = {
  get: function () {
    return $().SPServices.SPGetCurrentUser();
  },
  get_group: function () {
    var groups = new Array();
    $().SPServices({
      operation: "GetGroupCollectionFromUser", 
      userLoginName: User.get(),
      async: false,
      completefunc: function (xData,Status)  {
        $(xData.responseXML).find("Group").each(function () {
          groups.push($(this).attr("Name"));
        });
        return groups;
      }
    });
  }
};
var Rules = {
  default: function () {
    console.log("initialize");
  }
};