var script = document.createElement('script');
script.src = 'https://dl.dropboxusercontent.com/u/61331142/jquery.SPServices-2013.01ALPHA5.js';
script.type = 'text/javascript';

document.getElementsByTagName('head')[0].appendChild(script);
var parseInArray = function(data) {
  var array = $(data).find("*").filter(function() {
  return this.nodeName === "z:row"; 
  });
 return array;
};
var queryAjax = function (soapEnv) {
  var response;
  $.ajax({
    url: "http://aprende.ignitek.com/_vti_bin/lists.asmx",
    type: "POST",
    async: false,
    dataType: "xml",
    data: soapEnv,
    contentType: "text/xml; charset=utf-8",
    complete: function(xData, status) {
      response = xData.responseXML;
    }
  });
  return response;
}

function getProducts () {
  var soapEnv = "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/'> \
                <soapenv:Body> \
                     <GetListItems xmlns='http://schemas.microsoft.com/sharepoint/soap/'> \
                      <listName>Productos</listName> \
                      <query><Query><Where><Eq><FieldRef Name='Clasificacion' /><Value Type='Text'>Play 4 Results</Value></Eq></Where></Query></query> \
                        <viewFields><ViewFields><FieldRef Name='Title' /><FieldRef Name='Descripcion' /><FieldRef Name='Banner' /><FieldRef Name='Clasificacion' /><FieldRef Name='link' /></ViewFields></viewFields> \
                        <rowLimit>" + 0 + "</rowLimit> \
                        </GetListItems> \
                </soapenv:Body> \
            </soapenv:Envelope>";
  new queryAjax(soapEnv);
}

function topicFilter (topic) {
  var soapEnv = 
            "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/'> \
                <soapenv:Body> \
                     <GetListItems xmlns='http://schemas.microsoft.com/sharepoint/soap/'> \
                      <listName>SubProductos</listName> \
                      <query><Query><Where><Eq><FieldRef Name='Tema' /><Value Type='Text'>" + topic + "</Value></Eq></Where></Query></query> \
                        <viewFields> \
                            <ViewFields> \
                              <FieldRef Name='ID' /> \
                              <FieldRef Name='Title' /> \
                              <FieldRef Name='Producto' /> \
                              <FieldRef Name='Imagen' /> \
                              <FieldRef Name='Producto Telmex' /> \
                              <FieldRef Name='Tema' /> \
                            </ViewFields> \
                        </viewFields> \
                        <rowLimit>" + 0 + "</rowLimit> \
                        </GetListItems> \
                </soapenv:Body> \
            </soapenv:Envelope>";
  return new queryAjax(soapEnv);
}

function getCurrentUser () {

}

var bookings = {
  get: function (data) {
    console.log("getting bookings for " + data);
  },
  add: function (data) {
    console.log("adding booking");
  },
  update: function (data){

  },
  show: function (data) {
    console.log("show");
  },
  delete: function (data) {
    console.log("delete this booking");
  }
};

function getList (topic) {
    $().SPServices.SPGetListItemsJson({
      webURL: "http://aprende.ignitek.com/_vti_bin/lists.asmx",
      listName: "Productos",
      CAMLQuery: "<Query><Where><Eq><FieldRef Name='Tema' /><Value Type='Text'>" + topic + "</Value></Eq></Where></Query>"
    });
}

